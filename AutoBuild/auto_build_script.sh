HOMEDIR=/home/pi
OUTPUT=/tmp/$$output.txt
UPDATE_CMD=update_all
#UPDATE_CMD=update_code

echo '' >$OUTPUT

cd $HOMEDIR/ReleaseMgmt_UnitTests/FrontEndKivy
fab localhost $UPDATE_CMD -p the_pi_password 2>> $OUTPUT
cd $HOMEDIR/ReleaseMgmt_UnitTests/BackEnd
fab localhost $UPDATE_CMD -p the_pi_password 2>> $OUTPUT
if [ -s $OUTPUT ]
then
    cd $HOMEDIR/ReleaseMgmt_UnitTests/AutoBuild
    ./send_email.py aware@buildinginlink.com 'Automatic build results' "`cat $OUTPUT`"
fi
rm -f $OUTPUT
