#!/usr/bin/env python
# -*- coding: iso8859-15 -*-                                                                                                            
import os,sys
import pprint
import logging
import smtplib
import socket
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir,'..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

configdata = {
        "email_server" : "smtp-relay.gmail.com",
        "email_port" : 587,
        "email_user" : "awarenotify@buildinglink.com",
        "hostname" : socket.gethostname(),
        "email_password" : "xXNYnd-WZ{^2tX}?QF=7TTZCu!8%aR!",
    }

def send_email(to_list,subject,body):
    message = MIMEMultipart()
    message['From'] = configdata['email_user']
    message['Subject'] = 'Host {0}:{1}'.format(configdata['hostname'],subject)
    message.preamble = ''
    body = MIMEText(body, _subtype="plain", _charset="utf-8")
    message.attach(body)
    SEND_TO = [ configdata['email_user'] ] + to_list
    message['To'] = COMMASPACE.join(SEND_TO)

    smtp = smtplib.SMTP(configdata['email_server'],configdata['email_port'])
    smtp.starttls()
    smtp.ehlo()
    smtp.login(configdata['email_user'],configdata['email_password'])
    smtp.sendmail(configdata['email_user'],SEND_TO,message.as_string())

def aware_email_alert(body):
    return send_email([],"Alert from {0}".format(hostname),body)

if __name__ == "__main__":
    to_list = [sys.argv[1]]
    subject = sys.argv[2]
    body = sys.argv[3]
    send_email(to_list,subject,body)

