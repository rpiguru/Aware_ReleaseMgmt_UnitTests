# -*- coding: iso8859-15 -*-

"""
    0. For brand new release, run these:                                                          

            GeneralPurpose/run_first.sh
            GeneralPurpose/initial_apt_LITE.sh
            cd GeneralPurpose && fab install_all

    1. Build

        - Local usage:

            fab localhost new_build
          or:
            fab localhost update_code

        - Remote usage:

            fab remote_host_or_ip new_build
          or:
            fab remote_host_or_ip update_code

"""
import os,sys
import time
import re

if os.geteuid() == 0:
    sys.exit("Don't run fabric as root. Log in as the pi user, same password.")   

PROJDIR=None

appdir = os.path.abspath(os.path.dirname(__file__))
if not PROJDIR:
    PROJDIR = os.path.abspath(os.path.join(appdir,'../..'))

if appdir not in sys.path:
    if not PROJDIR:
        sys.path.append(PROJDIR)
    sys.path.append(appdir)

import re
import json
import socket
import uuid
from fabric.api import local, run, task, env, warn_only, cd
from fabric.colors import cyan,magenta
from fabric.operations import sudo

hostname = socket.gethostname()

override_name = re.sub('\.','_',hostname)
override_name = re.sub('\-','_',override_name)
override_name = re.sub(' ','_',override_name)
host_config_override = './build_{0}.config'.format(override_name)

#Read configuration file. Allow host name override for testing.
try:
    config_filename = os.path.join(appdir,host_config_override)
    config_data=open(config_filename).read()
except:
    config_filename = os.path.join(appdir,"build.config")
    config_data=open(config_filename).read()

configdata = json.loads(config_data)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Change these to match your project.
#
# Assume project directory is above this repo directory, unless
# otherwise hard-coded.
#
#PROJDIR="/home/pi"
REPONAME='AwareService'
SERVICENAME='awareservice'
PIP='pip3'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BRANCH=configdata['branch']
ENVTYPE=configdata['envtype']

RUNDIR='/opt/{0}/bin'.format(SERVICENAME)
LOGDIR='/var/log/aware' # All Aware service logs go here.

CODEDIR="{0}/{1}".format(PROJDIR,REPONAME)

def local_run(*args):
    return local(args[0], capture=True)


@task
def localhost():
    """
    Set localhost as the current environment
    """
    env.run = local_run
    env.hosts = ['localhost']


@task
def remote(host='', username='pi', password='raspberry', key_file=''):
    """
    Set remote host as the current environment
    """
    env.run = run
    env.hosts = [host]
    env.user = username
    env.password = password
    # or, specify path to server public key here:
    # env.key_filename = key_file

@task
def get_source_code():
    if os.path.isdir(CODEDIR):
        with cd(CODEDIR):
            print(cyan('Getting latest front end code from {0} branch to this directory: {1}'.format(BRANCH,CODEDIR)))
            run('git branch')
            run('git pull')
            #run('git branch --set-upstream-to origin/{0} {0}'.format(BRANCH))
            #run('git pull origin {0}'.format(BRANCH))
    else:
        with cd(PROJDIR):
            print(cyan('Cloning front end code from {0} branch to this directory: {1}'.format(BRANCH,CODEDIR)))
            run('git clone -b {0} https://github.com/BuildingLinkAware/{1}'.format(BRANCH,REPONAME))

@task
def start_service():
    sudo('systemctl start awareservice')

@task
def generate_mqtt_passwd():
    print(magenta('Generating MQTT credentials...'))
    file = open('/proc/cpuinfo', "r")
    for line in file:
        if re.search('Serial', line):
            gid = line.split(':')[1].strip()
            gid = gid.upper()
            break
    file.close()
    gid = str(int(time.time())) + '_' + gid.lstrip('0')
    mqtt_passwd=str(uuid.uuid4().hex.upper())
    outstr = '"mqtt_name" : "{0}",'.format(gid)
    sudo("sed -i 's/\"mqtt_name\".*$/{0}/' {1}/Application/awareservice.config".format(outstr,CODEDIR))
    outstr = '"mqtt_password" : "{0}",'.format(mqtt_passwd)
    sudo("sed -i 's/\"mqtt_password\".*$/{0}/' {1}/Application/awareservice.config".format(outstr,CODEDIR))
    sudo('cp -r {0}/Application/awareservice.config {1}'.format(CODEDIR,RUNDIR))
    print(magenta("This name {0} and password {1} have to be registered in the local mosquitto password registry.".format(gid,mqtt_passwd)))

@task
def install_app_services():
    print(magenta('App related prep...'))
    sudo('mkdir -p {0} && chmod 775 {0}'.format(RUNDIR))
    with warn_only():
        sudo('mkdir -p {0} && chmod 775 {0}'.format(LOGDIR))
    sudo('cp -r {0}/Application/* {1}'.format(CODEDIR,RUNDIR))
    sudo('cp -r {0}/VERSION {1}'.format(CODEDIR,RUNDIR))
    sudo('cp {0}/system/{1}.service /etc/systemd/system'.format(appdir,SERVICENAME))
    sudo('systemctl enable {0}'.format(SERVICENAME)) 

@task
def reload_services():
    print(magenta('Reloading code and app services...'))
    with warn_only():
        sudo('mkdir -p {0} && chmod 775 {0}'.format(RUNDIR))
        sudo('mkdir -p {0} && chmod 775 {0}'.format(LOGDIR))
    sudo('cp -r {0}/Application/* {1}'.format(CODEDIR,RUNDIR))
    sudo('cp -r {0}/VERSION {1}'.format(CODEDIR,RUNDIR))
    sudo('cp {0}/system/{1}.service /etc/systemd/system'.format(appdir,SERVICENAME))
    sudo('systemctl daemon-reload')
    sudo('systemctl enable {0}'.format(SERVICENAME))
    sudo('systemctl stop {0} && systemctl start {0}'.format(SERVICENAME)) 


@task
def install_python_deps():
    print(magenta('Installing python packages...'))
    sudo('{0} install -U raven pymongo pyserial requests xbee paho-mqtt uptime setproctitle Pygtail ibmiotf gevent flask flask-restful pyyaml requests-oauthlib'.format(PIP))

@task
def current_version():
    with cd(CODEDIR):
        run('hostnamectl > VERSION')
        run('echo "git revision:" `git log -1 --oneline` >> VERSION')

@task
def current_manifest():
    sudo('echo "Date:" `TZ=UTC && date` > {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "Hardware info:" `/opt/vc/bin/vcgencmd version` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "OS release:"    `/usr/bin/lsb_release -a`  >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "eth0:"          `/sbin/ifconfig eth0 | grep HWaddr` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "wlan0:"         `/sbin/ifconfig wlan0 | grep HWaddr` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "Back End:"      `cat /opt/awareservice/bin/VERSION` >> {0}/MANIFEST'.format(PROJDIR))
    with warn_only():
        sudo('echo "Front End:"     `cat /opt/awaregui/bin/VERSION` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "Pip:"       `{1} list` >> {0}/MANIFEST'.format(PROJDIR,PIP)) 

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@task
def new_build():
    print(magenta('CONFIG FILE NAME: {0}, BRANCH: {1}, ENV TYPE: {2}'.format(config_filename,BRANCH,ENVTYPE)))
    get_source_code()
    install_python_deps()
    current_version()
    install_app_services()
    current_manifest()

@task
def update_code():
    print(magenta('CONFIG FILE NAME: {0}, BRANCH: {1}, ENV TYPE: {2}'.format(config_filename,BRANCH,ENVTYPE)))
    install_python_deps()
    get_source_code()
    current_version()
    reload_services()
    current_manifest()

@task
def update_all():
    print(magenta('CONFIG FILE NAME: {0}, BRANCH: {1}, ENV TYPE: {2}'.format(config_filename,BRANCH,ENVTYPE)))
    sudo('sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y')
    sudo('rpi-update')
    install_python_deps()
    get_source_code()
    current_version()
    reload_services()
    current_manifest()            
    sudo('reboot')            
