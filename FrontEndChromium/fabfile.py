# -*- coding: iso8859-15 -*-

import os,sys

PROJDIR=None

if os.geteuid() == 0:
    sys.exit("Don't run fabric as root. Log in as the pi user, same password.")

appdir = os.path.abspath(os.path.dirname(__file__))
if not PROJDIR:
    PROJDIR = os.path.abspath(os.path.join(appdir,'../..'))

if appdir not in sys.path:
    if not PROJDIR:
        sys.path.append(PROJDIR)
    sys.path.append(appdir)

import re
import json
import socket
from fabric.api import local, run, task, env, warn_only, cd
from fabric.colors import cyan
from fabric.operations import sudo

hostname = socket.gethostname()

override_name = re.sub('\.','_',hostname)
override_name = re.sub('\-','_',override_name)
override_name = re.sub(' ','_',override_name)
host_config_override = './build_{0}.config'.format(override_name)

#Read configuration file. Allow host name override for testing.
try:
    config_filename = os.path.join(appdir,host_config_override)
    config_data=open(config_filename).read()
except:
    config_filename = os.path.join(appdir,"build.config")
    config_data=open(config_filename).read()

configdata = json.loads(config_data)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Change these to match your project.
#
# Assume project directory is above this repo directory, unless
# otherwise hard-coded.
#
#PROJDIR="/home/pi"
REPONAME='AwareGui'
SERVICENAME='awaregui_chromium'
PIP='pip3'
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BRANCH=configdata['branch']
ENVTYPE=configdata['envtype']

RUNDIR='/opt/{0}/bin'.format(SERVICENAME)
LOGDIR='/var/log/aware' # All Aware service logs go here.

CODEDIR="{0}/{1}".format(PROJDIR,REPONAME)

def local_run(*args):
    return local(args[0], capture=True)


@task
def localhost():
    """
    Set localhost as the current environment
    """
    env.run = local_run
    env.hosts = ['localhost']


@task
def remote(host='', username='pi', password='raspberry', key_file=''):
    """
    Set remote host as the current environment
    """
    env.run = run
    env.hosts = [host]
    env.user = username
    env.password = password
    # or, specify path to server public key here:
    # env.key_filename = key_file


@task
def get_source_code():
    if os.path.isdir(CODEDIR):
        with cd(CODEDIR):
            print(cyan('Getting latest front end code from {0} branch to this directory: {1}'.format(BRANCH,CODEDIR)))
            run('git branch')
            run('git pull')
            #run('git branch --set-upstream-to origin/{0} {0}'.format(BRANCH))
            #run('git pull origin {0}'.format(BRANCH))
    else:
        with cd(PROJDIR):
            print(cyan('Cloning front end code from {0} branch to this directory: {1}'.format(BRANCH,CODEDIR)))
            run('git clone -b {0} https://github.com/BuildingLinkAware/{1}'.format(BRANCH,REPONAME))

@task
def install_os_deps():
    """
    Install OS dependencies of AwareGui application
    """
    sudo('apt-get update')
    sudo('apt-get install libgconf-2-4 libxss1 libxtst6 -y')
    sudo('dpkg-reconfigure x11-common')
    sudo('apt-get remove nodered -y')
    sudo('apt-get remove nodejs nodejs-legacy -y')
    sudo('apt-get remove npm  -y')
    sudo('curl -sL https://deb.nodesource.com/setup_8.x | sudo bash - ')
    sudo('apt-get install nodejs -y')
    sudo('npm cache clean --force')
    sudo('npm install -g npm')

@task
def install_splash_screen():
    print(cyan('Setting up BuildingLink logo...'))
    sudo('apt-get -y install fbi libyaml-dev')
    sudo('cp {0}/Application/assets/images/aware_logo_fullsize.png /etc/splash.png'.format(CODEDIR))
    sudo('cp {0}/Application/extra/asplashscreen /etc/init.d/asplashscreen'.format(CODEDIR))
    sudo('chmod a+x /etc/init.d/asplashscreen')
    sudo('insserv /etc/init.d/asplashscreen')

@task
def apply_os_config():
    print(cyan('Increasing GPU memory size...'))
    sudo('echo "gpu_mem=384" | tee -a /boot/config.txt')

@task
def install_system_deps():
    print(cyan('Installation system deps...'))
    sudo('cp {0}/system/{1}.service /etc/systemd/system'.format(appdir,SERVICENAME))
    with warn_only():
        sudo('mkdir -p {0} && chmod 775 {0}'.format(LOGDIR))
    sudo('mkdir -p {0} && chmod 775 {0}'.format(RUNDIR))
    sudo('cp -r {0}/Chromium_Application/* {1}'.format(CODEDIR,RUNDIR))
    sudo('cp -r {0}/VERSION {1}'.format(CODEDIR,RUNDIR))
    with cd('{0}'.format(RUNDIR)):
        sudo('npm install --unsafe-perm')
    sudo('systemctl enable {0}'.format(SERVICENAME))

@task
def start_service():
    sudo('systemctl start {0}'.format(SERVICENAME))

@task
def reload_services():
    print(cyan('Release area update...'))
    with warn_only():
        sudo('cp {0}/system/{1}.service /etc/systemd/system'.format(appdir,SERVICENAME))
        sudo('mkdir -p {0} && chmod 775 {0}'.format(LOGDIR))
    sudo('cp -r {0}/Chromium_Application/* {1}'.format(CODEDIR,RUNDIR))
    with cd('{0}'.format(RUNDIR)):
        run('npm install')
    sudo('cp -r {0}/VERSION {1}'.format(CODEDIR,RUNDIR))
    sudo('systemctl daemon-reload')
    sudo('systemctl enable {0}'.format(SERVICENAME))
    sudo('systemctl stop {0} && systemctl start {0}'.format(SERVICENAME))

@task
def current_version():
    with cd(CODEDIR):
        run('hostnamectl > VERSION')
        run('echo "git revision:" `git log -1 --oneline` >> VERSION') 

@task
def current_manifest():
    sudo('echo "Date:" `TZ=UTC && date` > {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "Hardware info:" `/opt/vc/bin/vcgencmd version` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "OS release:"    `/usr/bin/lsb_release -a`  >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "eth0:"          `/sbin/ifconfig eth0 | grep HWaddr` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "wlan0:"         `/sbin/ifconfig wlan0 | grep HWaddr` >> {0}/MANIFEST'.format(PROJDIR))
    with warn_only():
        sudo('echo "Back End:"      `cat /opt/awareservice/bin/VERSION` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "Front End:"     `cat /opt/awaregui/bin/VERSION` >> {0}/MANIFEST'.format(PROJDIR))
    sudo('echo "Pip:"       `{1} list` >> {0}/MANIFEST'.format(PROJDIR,PIP))
    sudo('echo "npm:"       `npm-list` >> {0}/MANIFEST'.format(PROJDIR))


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@task
def new_build():
    print(cyan('CONFIG FILE NAME: {0}, BRANCH: {1}, ENV TYPE: {2}'.format(config_filename,BRANCH,ENVTYPE)))
    apply_os_config()
    get_source_code()
    install_splash_screen()
    install_os_deps()
    current_version()
    install_system_deps()
    current_manifest()
    #start_service()

@task
def update_code():
    print(cyan('CONFIG FILE NAME: {0}, BRANCH: {1}, ENV TYPE: {2}'.format(config_filename,BRANCH,ENVTYPE)))
    get_source_code()
    current_version()
    reload_services()
    current_manifest()

@task
def update_all():
    print(cyan('CONFIG FILE NAME: {0}, BRANCH: {1}, ENV TYPE: {2}'.format(config_filename,BRANCH,ENVTYPE)))
    sudo('sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y')
    sudo('rpi-update')
    get_source_code()
    current_version()
    reload_services()
    current_manifest()
    sudo('reboot')
