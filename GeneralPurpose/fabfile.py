# -*- coding: iso8859-15 -*-
  
"""
   0. For brand new release, run these:                                                                                
            GeneralPurpose/run_first.sh
            GeneralPurpose/initial_apt_LITE.sh
            cd GeneralPurpose && fab install_all

"""
import os,sys

if os.geteuid() == 0:
    sys.exit("Don't run fabric as root. Log in as the pi user, same password.")   

from fabric.api import local, run, task, env, warn_only, cd
from fabric.utils import puts
from fabric.colors import cyan,magenta
from fabric.operations import sudo, prompt

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Change these to match your project.
#
# Assume project directory is above this repo directory, unless
# otherwise hard-coded.
#
#PROJDIR="/home/pi"
PROJDIR=None
#REPONAME='AwareGui'
BRANCH='master'
#SERVICENAME='awaregui'
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

appdir = os.path.abspath(os.path.dirname(__file__))
if not PROJDIR:
    PROJDIR = os.path.abspath(os.path.join(appdir,'../..'))

if appdir not in sys.path:
    if not PROJDIR:
        sys.path.append(PROJDIR)
    sys.path.append(appdir)

def local_run(*args):
    return local(args[0], capture=True)

@task
def localhost():
    """
    Set localhost as the current environment
    """
    env.run = local_run
    env.hosts = ['localhost']


@task
def remote(host='', username='pi', password='raspberry', key_file=''):
    """
    Set remote host as the current environment
    """
    env.run = run
    env.hosts = [host]
    env.user = username
    env.password = password
    # or, specify path to server public key here:
    # env.key_filename = key_file


@task
def install_pi_settings():
    print(magenta('Pi Settings...'))
    with warn_only():
        sudo('crontab -l > /tmp/mycron')

    sudo('cp {0}/system/ntp_sync.sh /root && echo "*/5 * * * * /root/ntp_sync.sh" >> /tmp/mycron && sudo crontab /tmp/mycron && rm /tmp/mycron'.format(appdir))
    sudo('cp {0}/system/time_api.py /root && echo "*/6 * * * * /root/time_api.py" >> /tmp/mycron && sudo crontab /tmp/mycron && rm /tmp/mycron'.format(appdir))
    sudo('dphys-swapfile swapoff && sudo dphys-swapfile uninstall && sudo update-rc.d dphys-swapfile remove')

@task
def backup_network_files():
    sudo('cp /etc/network/interfaces {0}'.format(PROJDIR))
    sudo('cp /etc/dhcpcd.conf {0}'.format(PROJDIR))
    sudo('cp /etc/wpa_supplicant/wpa_supplicant.conf {0}'.format(PROJDIR))

@task
def install_os_deps():
    print(magenta('OS dependencies'))
    sudo('apt-get -y autoremove')
    sudo('apt-get -y install openntpd mosquitto matchbox-keyboard bc cmake mongodb-server weavedconnectd')

@task
def install_python_deps():
    print(magenta('Installing python packages...'))
    sudo('pip3 install -U pyserial raven requests xbee paho-mqtt setproctitle Pygtail pymongo uptime')

@task
def reboot():
    sudo('reboot')

@task
def install_nmcli_only():
    sudo('apt-get install network-manager -y')
    sudo('cp {0}/system/NetworkManager.conf /etc/NetworkManager/NetworkManager.conf'.format(appdir))
    sudo('cp {0}/system/interfaces /etc/network/interfaces'.format(appdir))
    prompt('Enter your wifi SSID',key='ssid',default=None)
    prompt('Enter your wifi password',key='wifi_password',default=None)
    with warn_only():
        sudo('crontab -l >/tmp/tempcron')
    sudo('echo "@reboot sleep 30 && nmcli device wifi connect {0} password {1}" >>/tmp/tempcron && echo "@reboot date >/tmp/ranit" >> /tmp/tempcron && crontab /tmp/tempcron && rm -f /tmp/tempcron'.format(env.ssid,env.wifi_password))
    sudo('crontab -l')
    print(magenta('If the crontab looks good, reboot now! Otherwise run sudo crontab -e.'))

@task
def install_nmcli_only_ethernet():
    sudo('apt-get install network-manager -y')
    sudo('cp {0}/system/NetworkManager.conf /etc/NetworkManager/NetworkManager.conf'.format(appdir))
    sudo('cp {0}/system/interfaces /etc/network/interfaces'.format(appdir))    
    print(magenta('nmcli successfully installed. Reboot the Pi.'))  

@task
def install_home_assistant():
    sudo('apt-get install python3 python3-venv python3-pip')
    sudo('useradd -rm homeassistant')
    sudo('cd /srv')
    sudo('mkdir homeassistant')
    sudo('chown homeassistant:homeassistant homeassistant')
    sudo('su -s /bin/bash homeassistant')


@task
def run_home_assistant():
    sudo('cd /srv/homeassistant')
    sudo('python3 -m venv .')
    sudo('source bin/activate')
    sudo('pip3 install homeassistant')
    print(magenta('Connect Zigbee/Zwave stick and find COM port, then interactively add zha'))
    print(magenta('####IMPORTANT#####  Go to http://localhost:8123 ##############'))
    print(magenta('Go here to add things network stick: https://home-assistant.io/components/zha/'))
    sudo('hass')

@task
def install_all():
    backup_network_files()
    install_pi_settings()
    install_os_deps()
    install_python_deps()
    reboot()
