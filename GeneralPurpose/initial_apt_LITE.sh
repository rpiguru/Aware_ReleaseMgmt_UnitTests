#!/usr/bin/env bash
sudo apt-get -y install lsb-core lsb-release
sudo apt-get -y install libffi-dev libssl-dev python3
sudo apt-get -y install --fix-missing xserver-xorg 
sudo apt-get -y install xinit lxterminal
sudo apt-get -y install chromium-browser
sudo apt-get -y install python3-setuptools python3-dev libpython3-all-dev
sudo apt-get -y install python-setuptools python-dev libpython-all-dev
sudo apt-get -y install git
sudo apt-get -y remove python3-pip
sudo apt-get -y remove python-pip
sudo easy_install3 pip
sudo easy_install pip
sudo pip3 install fabric3
sudo update-rc.d ssh defaults
sudo touch /boot/ssh
/etc/init.d/ssh start
dpkg-reconfigure locales
dpkg-reconfigure keyboard-configuration
systemctl stop serial-getty@ttyS0.service && systemctl disable serial-getty@ttyS0.service && systemctl mask serial-getty@ttyS0.service
echo "lcd_rotate=2" | tee -a /boot/config.txt
echo "enable_uart=1" | tee -a /boot/config.txt
sudo sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sudo apt-get install network-manager -y
sudo cp ./system/NetworkManager.conf /etc/NetworkManager/NetworkManager.conf
sudo cp ./system/interfaces /etc/network/interfaces
#sudo service network-manager restart

cat << LONG_OUTPUT

    These commands are manual, and are required next:
    (1) As the "pi" user, type:

        passwd

        and enter the new default password.

    (2) Type:

        sudo su - 

        Then type:

        passwd

        Set the root password above to the same one you used for the "pi" user.

    (3) Type:

        sudo reboot

    (4) After rebooting, if you are on wifi, type: 

        sudo nmcli device wifi connect your_ssid password your_wifi_password

        See the Network Commands section of our wiki page:

        https://github.com/BuildingLinkAware/ReleaseMgmt_UnitTests

LONG_OUTPUT
