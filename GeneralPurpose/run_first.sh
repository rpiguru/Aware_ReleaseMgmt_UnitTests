sudo apt-get -y update
sudo apt-get -y dist-upgrade
sudo rpi-update
sudo rm -rf /etc/ssh/ssh_host_* && sudo dpkg-reconfigure openssh-server # necessary when duplicating OS images.
sudo touch /boot/ssh
# setup for usb GSM modem stick.
sudo echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list.d/backports.list
gpg --keyserver pgpkeys.mit.edu --recv-key 8B48AD6246925553   
gpg -a --export 8B48AD6246925553 | sudo apt-key add -
gpg --keyserver pgpkeys.mit.edu --recv-key 7638D0442B90D010     
gpg -a --export 7638D0442B90D010 | sudo apt-key add -
sudo apt-get -t jessie-backports -y install usb-modeswitch usb-modeswitch-data
echo "Rebooting...."
sleep 10
sudo reboot
