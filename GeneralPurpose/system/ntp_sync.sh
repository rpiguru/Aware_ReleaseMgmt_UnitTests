!/bin/bash                                                                     
still_running=`ps awux | grep 'openntpd -s' | awk '{print $2}'`
if [[ ! -z  $still_running  ]]
then
    for x in $still_running
    do
        kill -9 $x
    done
else
    /usr/sbin/openntpd -s
fi

