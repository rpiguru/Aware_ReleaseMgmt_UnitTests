#!/usr/bin/env python

import subprocess
import json

#API limited by 1 call per second

process  = subprocess.Popen("curl \"http://api.timezonedb.com/v2/get-time-zone?key=Z9HHY9J602J2&format=json&by=zone&zone=UTC\"",shell=True,stdout=subprocess.PIPE)
out,err = process.communicate()
time = json.loads(out)
time = time["formatted"]+".000"
process2  = subprocess.Popen("sudo date --set=\"{0} UTC\"".format(time),shell=True,stdout=subprocess.PIPE)
out,err = process2.communicate()                                                
