# Build Configuration:

The build.config file in each project directory 
(FrontEndKive, BackEnd, etc) determines which brnach you're building, and 
whether debug is on/off. 

build.config clones the production branch, and turns all debug off. 

Override this by creting a build_<hostname>.config file, with different
settings. Spaces and hyphens in hostname are converted to underscores.

Once creating the build_<hostname>.config file and saving new settings,
remove the /home/pi/Aware* code directories created by the original 
build, if they exist. Then rerun the build commands below.



## For New Releases, from-scratch:

1: Write the latest LITE OS to your sim card
2: sudo apt-get install git
3: git clone https://github.com/BuildingLinkAware/ReleaseMgmt_UnitTests
4: cd to GeneralPurpose directory
5: Run scripts in this order. Note that a reboot occurs:
    sudo ./run_first.sh 
    sudo ./initial_apt_LITE.sh
    sudo fab install_all
6: cd to BackEnd. Run fab new_build
7: cd to FrontEndKivy. Run fab new_build



## For Existing Releases, to update code:
 
1: cd BackEnd. run fab update_code
2: cd FrontEndKivy. run fab update_code

For Existing Releases, to update code, OS, and firmware:
 
1: cd BackEnd. run fab update_all
2: cd FrontEndKivy. run fab update_all

After doing either a new release or an update, see the MANIFEST file for a list of hardware
and software components contained in the release.



## To automate a build/update, for our internal test machines:

1: After a successful install, cd to AwareService. Type:
    
    git config credential.helper store

2: Do a git pull. Enter username and password. It is saved from this point forward for this repo.
3: Repeat these steps for AwareGui
4: Activate the crontab in the ReleaseMgmt_UnitTests/AutoBuild directory



## Network commands (for wifi only, none necessary for Ethernet)

Connect to wifi: sudo nmcli device wifi connect your_ssid password your_wifi_password

Disconnect: sudo nmcli device disconnect wlan0

Bring wifi up/down: sudo nmcli connection up/down your_wifi_ssid

or:

sudo nmcli radio wifi on/off

Cheat sheet: https://fedoraproject.org/wiki/Networking/CLI
